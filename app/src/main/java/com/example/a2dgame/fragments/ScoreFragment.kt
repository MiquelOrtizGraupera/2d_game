package com.example.a2dgame.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentResultListener
import androidx.navigation.fragment.findNavController
import com.example.a2dgame.R
import com.example.a2dgame.databinding.FragmentScoreBinding


class ScoreFragment : Fragment() {
    private lateinit var binding: FragmentScoreBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentScoreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.score.text = arguments?.getInt("score").toString()

        binding.btnPlayAgain.setOnClickListener {
            findNavController().navigate(R.id.gameFragment)
        }
        binding.btnExit.setOnClickListener {
            findNavController().navigate(R.id.logInFragment)
        }
    }
}