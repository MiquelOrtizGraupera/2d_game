package com.example.a2dgame.fragments

import android.graphics.Point
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.example.a2dgame.GameView
import com.example.a2dgame.databinding.FragmentGameBinding

class GameFragment : Fragment() {

    private lateinit var binding: FragmentGameBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val game: FrameLayout = FrameLayout(requireContext())
        val display = requireActivity().windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val gameView = GameView(requireContext(), size)
        game.addView(gameView)
        return game
    }


}