package com.example.a2dgame


import android.content.Context
import android.graphics.*
import android.media.MediaPlayer
import android.media.SoundPool
import android.view.MotionEvent
import android.view.SurfaceView
import androidx.navigation.findNavController
import com.example.a2dgame.dataClass.Bullet
import com.example.a2dgame.dataClass.Enemy
import com.example.a2dgame.dataClass.Explosion
import com.example.a2dgame.dataClass.SpaceShip
import com.example.a2dgame.fragments.GameFragmentDirections
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class GameView (context:Context, val size:Point) : SurfaceView(context){

    var enemy = Enemy(context, size.x, size.y, 0f)
    var player = SpaceShip(context, size.x, size.y)
    var playing = true
    var canvas: Canvas = Canvas()
    val paint: Paint = Paint()
    var background : Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.spacebg)
    val screenWidth = resources.displayMetrics.widthPixels
    val screenHeight = resources.displayMetrics.heightPixels
    var shootAction = true
    var isBegining = true
    var activeBullets = mutableListOf<Bullet>()
    var playerYShooter = mutableListOf<Float>()
    var enemyList = mutableListOf<Enemy>()
    var intersect:Boolean = false
    var coordIntersect = mutableListOf<Float>()
    var explosion = Explosion(context)
    var colision = false
    var score = 0
    var mediaPlayer: MediaPlayer? = MediaPlayer.create(context, R.raw.duality)
    val soundPool = SoundPool.Builder().setMaxStreams(5).build()
    val shotMade = soundPool.load(context, R.raw.lasershoot, 0)
    val explosionSound = soundPool.load(context, R.raw.explosion, 0)

    init {
        mediaPlayer?.start()
        startGame()
    }

    fun startGame(){
        CoroutineScope(Dispatchers.Main).launch{
        while(playing){
                draw()
                colision()
                bulletImpact()
                update()
                delay(100)
            }
            println("PLAYING FALSE")
            mediaPlayer?.stop()
            val action = GameFragmentDirections.actionGameFragmentToScoreFragment()
            findNavController().navigate(action)
        }
    }

    fun playSound(id: Int){
        soundPool.play(id, 1f, 1f, 0, 0, 1f)
    }


    fun draw(){
        if(player.life == 0 || score < 0){
            playing = false
        }
        if (holder.surface.isValid) {
            canvas = holder.lockCanvas()
            //Background
            canvas.drawBitmap(background, 0f, 0f, paint)
            //SCORE
            paint.color = Color.YELLOW
            paint.textSize = 60f
            paint.textAlign = Paint.Align.RIGHT
            canvas.drawText("Score: $score", (size.x - paint.descent()), 75f, paint)
            //LIFE
            paint.color = Color.RED
            paint.textSize = 60f
            paint.textAlign = Paint.Align.LEFT
            canvas.drawText("Life: ${player.life}", 30f,75f, paint)
            //Enemy
            if (isBegining){
                canvas.drawBitmap(enemy.bitmap, enemy.positionX, 0f , paint)
            }else if(enemyList.size < 20){
                for ( enemy in enemyList){
                    canvas.drawBitmap(enemy.bitmap, enemy.positionX, enemy.positionY , paint)
                }
            }
            if(coordIntersect.size>0){
                canvas.drawBitmap(explosion.bitmap,coordIntersect[0], coordIntersect[1], paint)
            }
            //Spaceship
            canvas.drawBitmap(player.bitmap, player.positionX, player.positionY, paint)
            //BULLET
            for((index,bullet) in activeBullets.withIndex()){
                canvas.drawBitmap(bullet.bitmap, bullet.coordenadaX, playerYShooter[index], paint)
            }
           if(coordIntersect.size>0){
                coordIntersect.clear()
            }
            holder.unlockCanvasAndPost(canvas)
        }
    }

    fun colision(){
        for((indexE, e) in enemyList.withIndex()){
            if(RectF.intersects(e.positionEnemy, player.positionSpaceShip)){
                enemyList.removeAt(indexE)
                score--
                player.updateLife()
                break;
            }
        }
    }

    fun bulletImpact(){
        for((indexE, e) in enemyList.withIndex()){
            for ((indexS, b) in activeBullets.withIndex()){
                /*  TESTEAR POSICION X PARA INTERSECT*/
                if(RectF.intersects(b.positionShot, e.positionEnemy )){
                    println("INTERSECCION")
                    println("Enemy position --> " + e.positionEnemy)
                    println("Bullet position --> " + b.positionShot)
                    println(enemyList.size)
                    enemyList.remove(e)
                    activeBullets.remove(b)
                    playerYShooter.removeAt(indexS)
                    println(enemyList.size)
                    intersect = true
                    coordIntersect.add(e.positionX)
                    coordIntersect.add(e.positionY)
                    score++
                    playSound(explosionSound)
                    break;
                }
            }
            if(intersect){
                intersect = false
                break;
            }
        }
    }

    fun update(){
        for(e in enemyList){
            e.updateEnemy()
        }
        player.updatePlayer()
        if(shootAction){
            for (bullet in activeBullets){
                bullet.updateMisile()
            }
        }
    }

    fun createEnemy(){
        if (isBegining){
            isBegining = false
        }
        val enemies = Enemy(context, size.x, size.y,0f)
        enemyList.add(enemies)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean{
        if (event != null) {
            when(event.action){
                // Aquí capturem els events i el codi que volem executar per cadascún
                MotionEvent.ACTION_DOWN -> {
                    if(event.x < screenWidth / 2 && event.y > screenHeight /2) {
                        player.speed = 25
                    }
                    else if (event.x < screenWidth / 2 && event.y < screenHeight /2) {
                        player.speed = -25
                    }
                    else if(event.x > screenWidth / 2){
                        var misile = Bullet(context, size.x, size.y, player.positionX, player.positionY)
                        activeBullets.add(misile)
                        playerYShooter.add(player.positionY)
                        playSound(shotMade)
                        createEnemy()
                    }
                }
                MotionEvent.ACTION_UP -> {
                    player.speed = 0
                }
            }
        }
        return true
    }


}