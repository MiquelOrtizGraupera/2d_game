package com.example.a2dgame.dataClass

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.a2dgame.R

class Explosion (context: Context) {
    var bitmap : Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.explosion)
    private val width =  150f
    private val height = 150f

    init {
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }
}