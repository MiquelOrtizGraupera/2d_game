package com.example.a2dgame.dataClass

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.a2dgame.R

class Bullet (context:Context, screenX:Int, screenY:Int, positionX:Float, positionY: Float) {
    var bitmap : Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.missle)
    private val width =  30f
    private val height = 30f
    var coordenadaX = positionX
    var coordenadaY = positionY
    var speed = 25
    var positionShot = RectF()

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)

        positionShot.left = coordenadaX
        positionShot.top = coordenadaY
        positionShot.right = positionShot.left + width
        positionShot.bottom = positionShot.top + height
    }

    fun updateMisile(){
        coordenadaX += speed
        positionShot.offset(speed.toFloat(), 0f)
        //println("Bullet -->$positionShot")
    }
}