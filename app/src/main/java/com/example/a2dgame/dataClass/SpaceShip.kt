package com.example.a2dgame.dataClass

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import android.widget.Space
import com.example.a2dgame.R

class SpaceShip (context: Context, screenX:Int, screenY:Int) {
    var bitmap : Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.spaceship)
    val width = screenX / 6f
    val height = screenY / 6f
    var positionX = 30f
    var positionY = 50f
    var speed = 0
    var life = 10
    var positionSpaceShip = RectF()

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
        positionSpaceShip.left = positionX
        positionSpaceShip.top = positionY
        positionSpaceShip.right = positionSpaceShip.left + width
        positionSpaceShip.bottom = positionSpaceShip.top + height
    }

    fun updatePlayer(){
        positionY += speed
    }

    fun updateLife(){
        life -= 5
    }
}