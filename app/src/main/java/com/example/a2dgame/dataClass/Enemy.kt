package com.example.a2dgame.dataClass

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.a2dgame.R

class Enemy (context: Context, screenX:Int, screenY:Int, var positionY:Float) {
    var bitmap : Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.evilsspaceschip)
    val width = screenX / 10f
    val height = screenY / 10f
    var positionX = screenX - width
    var speed = 15
    var positionEnemy = RectF()

    init{
        positionY = (0..600).random().toFloat()
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
        positionEnemy.left = positionX
        positionEnemy.top = positionY
        positionEnemy.right = positionEnemy.left + width
        positionEnemy.bottom = positionEnemy.top + height

    }

    fun updateEnemy(){
        positionX -= speed
        positionEnemy.offset(-speed.toFloat(), 0f)
    }
}