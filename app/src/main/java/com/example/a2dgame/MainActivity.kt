package com.example.a2dgame

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.a2dgame.databinding.ActivityMainBinding
import com.example.a2dgame.fragments.LogInFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}